# -*- coding: utf-8 -*-
from flask import Flask
from flask import jsonify
from flask import request
import sqlite3
import json


app = Flask(__name__)

@app.route("/funcionario/<int:cpf>", methods=["GET"])
def ConsultarFuncionario(cpf):
    try:
        conn = sqlite3.connect('Funcionario.db')
        cursor = conn.cursor()
               
        cursor.execute("SELECT * FROM Funcionario WHERE cpf = ?",(str(cpf),))
        data = cursor.fetchall()
        cursor.close()
        conn.close()
        retorno = {}
        retorno['Nome'] = str(data[0][1])
        retorno['Cpf'] = str(data[0][0])
        retorno['Cargo'] = str(data[0][2])
        return json.dumps(retorno)

    except IndexError as x:
        return 'Não foi possivel selecionar dados'

@app.route("/funcionario/", methods=["POST"])
def CadastrarFuncionario():
    try:
        conn = sqlite3.connect('Funcionario.db',timeout=1)
        cursor = conn.cursor()

       
        Cargo = request.json["Cargo"]
        Nome = request.json["Nome"]
        Cpf = request.json["Cpf"]
        cursor.execute("INSERT INTO Funcionario(cpf,nome,cargo) VALUES('{0}','{1}','{2}')".format(Cpf,Nome,Cargo))
        conn.commit()
                
        cursor.close()
        conn.close()
        retorno = {'status':200,'mensagem':'Funcionario cadastrado com sucesso!'}
        return json.dumps(retorno)

    except sqlite3.OperationalError:
        print("database")
        return 'Não foi possivel cadastras o funcionario'

@app.route("/funcionario/<int:cpf>", methods=["DELETE"])
def DeletaFuncionario(cpf):
    try:
        conn = sqlite3.connect('Funcionario.db')
        cursor = conn.cursor()

        cursor.execute("DELETE FROM Funcionario WHERE cpf = '{0}'".format(cpf))
        conn.commit()
                
        cursor.close()
        conn.close()
        retorno = {'status':200,'mensagem':'Funcionario deletado com sucesso!'}
        return json.dumps(retorno)

    except IndexError as x:
        return 'Não foi possivel deletar o funcionario'
                                          
@app.route("/UpdateFuncionario/<int:cpf>", methods=["POST"])
def AtualizaFuncionario(cpf):
    try:
        conn = sqlite3.connect('Funcionario.db')
        cursor = conn.cursor()
        Cargo = request.json["Cargo"]
        Nome = request.json["Nome"]
        Cpf = request.json["Cpf"]
        cursor.execute("UPDATE Funcionario SET nome = '{1}', cargo = '{2}' WHERE cpf = '{0}'".format(Cpf,Nome,Cargo))
        conn.commit()
                
        cursor.close()
        conn.close()
        retorno = {'status':200,'mensagem':'Funcionario atualizado com sucesso!'}
        return json.dumps(retorno)

    except IndexError as x:
        return 'Não foi possivel atualizar o usuario'
                                          
if __name__ == "__main__":
    app.run(port=5001, debug=True)    
