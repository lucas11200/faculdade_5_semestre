
# -*- coding: utf-8 -*-
from flask import request as req
from flask import Flask
import requests as consulta
import json

sair = False

while not sair:
    print("\nA - Cadastrar funcionário")
    print("B - Alterar funcionário")
    print("C - Excluir funcionário")
    print("D - Selecionar funcionário")
    print("X - Sair\n")
    valor = input()
    if valor in ["A", "a"]:
        print("\nCADASTRO DE FUNCIONÁRIO\n")
        print("Digite o nome:")
        nome = input("Name:")
        print()
        Cpf = input("Cpf:")
        print("Digite o cargo:")
        cargo = input("cargo:")
        url = "http://localhost:5001/funcionario/"
        dados = {'Cpf': Cpf,'Nome': nome,  'Cargo': cargo}
        data = json.dumps(dados)
        
        retorno = consulta.api.post(url,data=None, json=dados)
        if retorno.status_code > 200:
            print("Cliente já existe...\n")
        else:
            print("Cliente cadastrado com sucesso!\n")

    elif valor in ["B", "b"]:
        print("\nAtualizando DE FUNCIONÁRIO\n")
        print("Digite o nome:")
        nome = input("Name:")
        print()
        Cpf = input("Cpf:")
        print("Digite o cargo:")
        cargo = input("cargo:")
        url1 = "http://localhost:5001/UpdateFuncionario/{0}".format(Cpf)
        dados = {'Cpf': Cpf,'Nome': nome,  'Cargo': cargo}
        data = json.dumps(dados)
        retorno = consulta.api.post(url1,data=None, json=dados)
        print(retorno)            

    elif valor in ["C", "c"]:
        print("\nEXCLUSÃO DE FUNCIONÁRIO\n")
        print("Digite o cpf:")
        cpf = input()
        
        url = "http://localhost:5001/funcionario/{0}".format(cpf)
        retorno = consulta.api.delete(url).json()
        print("Funcionário deletado com sucesso!\n")

        
    elif valor in ["D", "d"]:
        print("\nSELECIONANDO FUNCIONÁRIO\n")
        print("Digite o cpf:")
        cpf = input()
        
        url = "http://localhost:5001/funcionario/{0}".format(cpf)
        retorno = consulta.api.get(url).json()
        print (retorno) 

    elif valor in ["X", "x"]:
        sair = True
    else:
        print("Opção inválida. Tente novamente.\n")



    